

var waypoint = new Waypoint({
  element: document.getElementById('waypoint'),
  handler: function(direction) {
    if (direction == "down") {
      // console.log("waypoint triggered down");
      this.element.className += " animate__animated animate__bounceIn";
    } else {
      // console.log("waypoint triggered up");
      this.element.className = "btn buttonRed btn-block";
    }
  },
  offset: 500
})
